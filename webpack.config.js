const webpack = require('webpack');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const providerPlug = new webpack.ProvidePlugin({
	riot: 'riot',
});

module.exports = {
	entry: {
		app: ['./src/index.js'],
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.html$/,
				use: {loader: 'html-loader'}
			},
			{
				test: require.resolve('moment'),
				use: [{
					loader: 'expose-loader',
					options: 'moment'
				}],
			},
			{
				test: /\.js?/,
				include: path.resolve(process.cwd(), './src'),
				loader: 'babel-loader',
			},
			{
				test: /\.scss$/,
				use: ['style-loader', { loader: 'css-loader', options: { importLoaders: 1 } }, 'sass-loader']
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader?limit=10000&mimetype=application/font-woff'
			},
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader?limit=10000&mimetype=application/octet-stream'
			},
			{
				test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader'
			},
			{
				test: /\.png(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader?mimetype=image/png'
			},
			{
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader?limit=10000&mimetype=image/svg+xml'
			}
		],
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist'),
	},
	devServer: {
		contentBase: './src',
		compress: true,
		inline: true,
		open: true,
		historyApiFallback: {
			index: 'index.html',
		},
		port: 9000,
	},
	plugins: [
		new UglifyJSPlugin({
			sourceMap: true,
			uglifyOptions: {ecma: 6},
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('production'),
		}),
		providerPlug,
	],
};

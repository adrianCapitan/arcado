import store, { cuponReducer } from './redux';

class AppService {
  initStore() {
    store.registerReducer('cupon', cuponReducer);
    store.createStore();
  }
}

export default new AppService();

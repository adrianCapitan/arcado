import { tag } from 'riot';
import template from './app.tag.html';
import appService from './app.service';
import './cupon/cupon.tag';
import './app.tag.scss';

function appTag() {
  appService.initStore();
}

tag('acs-app', template, '', '', appTag);

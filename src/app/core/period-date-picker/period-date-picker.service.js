import moment from 'moment';

class PeriodDatePickerService {
  getStartDateConfig(startDate) {
    return {
      date: startDate || moment(),
      min: moment(),
      max: moment().add(1, 'days'),
    };
  }

  getEndDateConfig(endDate) {
    return {
      date: endDate || moment().add(1, 'days'),
      min: moment().add(1, 'days'),
      max: moment().add(1, 'years'),
    };
  }
}

export default new PeriodDatePickerService();

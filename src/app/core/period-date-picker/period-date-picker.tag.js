import { tag } from 'riot';
import 'riotgear/dist/rg-date/rg-date';
import template from './period-date-picker.tag.html';
import periodDatePickerService from './period-date-picker.service';

function periodDatePickerTag() {
  this.startDateConfig = periodDatePickerService.getStartDateConfig(this.opts.value.startDate);
  this.endDateConfig = periodDatePickerService.getEndDateConfig(this.opts.value.endDate);

  const isValidStartDate = () => this.startDateConfig.date.isBefore(this.endDateConfig.date);

  const isValidEndDate = () => this.endDateConfig.date.isAfter(this.startDateConfig.date);

  const triggerUpdate = () => {
    if (this.opts.onUpdate) {
      this.opts.onUpdate({
        startDate: this.startDateConfig.date,
        endDate: this.endDateConfig.date,
      });
    }
  };

  this.on('mount', () => {
    let prevStartDate = this.startDateConfig.date;
    let prevEndDate = this.endDateConfig.date;
    this.tags['rg-date'][0].on('select', () => {
      if (isValidStartDate()) {
        prevStartDate = this.startDateConfig.date;
      } else {
        this.startDateConfig.date = prevStartDate;
      }
      triggerUpdate();
    });

    this.tags['rg-date'][1].on('select', () => {
      if (isValidEndDate()) {
        prevEndDate = this.endDateConfig.date;
      } else {
        this.endDateConfig.date = prevEndDate;
      }
      triggerUpdate();
    });
  });
}

tag('acs-period-date-picker', template, '', '', periodDatePickerTag);

export default function joiErrorToObject(joiError) {
  return joiError.details.reduce((accumulator, currentValue) => {
    const newValue = {};
    newValue[currentValue.path[0]] = {
      message: currentValue.message,
    };
    return {
      ...newValue,
      ...accumulator,
    };
  }, {});
}

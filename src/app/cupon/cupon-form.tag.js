import { tag } from 'riot';
import 'riotgear/dist/rg-toggle/rg-toggle';
import template from './cupon-form.tag.html';
import '../core/period-date-picker/period-date-picker.tag';
import cuponFormService from './cupon-form.service';
import './cupon-form.tag.scss';

function cuponFormTag() {
  this.cupon = this.opts.cupon || cuponFormService.getDefaultValue();

  this.toggleStatusConfig = cuponFormService.getStatusToggleConfig(this.cupon.status);
  this.validation = {};

  const validate = () => {
    this.validation = cuponFormService.validateCupon(this.cupon);
  };

  const triggerUpdate = () => {
    if (this.opts.onUpdate) {
      this.opts.onUpdate({
        data: this.cupon,
        validation: this.validation,
      });
    }
  };

  this.onPeriodChange = (value) => {
    this.cupon.period = value;
    validate();
    this.update();
    triggerUpdate();
  };

  this.onStatusChange = (event) => {
    this.cupon.status = event.target.checked;
    validate();
    triggerUpdate();
  };

  this.onValueChange = name => ((event) => {
    this.cupon[name] = event.target.value;
    validate();
    triggerUpdate();
  });
}

tag('acs-cupon-form', template, '', '', cuponFormTag);

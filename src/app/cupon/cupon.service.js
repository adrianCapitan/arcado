import moment from 'moment';
import store, { saveCupon as saveCuponAction, setCupons } from '../redux';

function sortCompareValue(left, right, sortDirection, sortColumn) {
  if (sortDirection === 'ASC') {
    return left[sortColumn] > right[sortColumn];
  }
  return right[sortColumn] > left[sortColumn];
}

function sortComparePeriod(left, right, sortDirection) {
  const leftStart = moment(left.period.startDate);
  if (sortDirection === 'ASC') {
    return leftStart.isAfter(right.period.startDate);
  }
  return leftStart.isBefore(right.period.startDate);
}

class CuponService {
  getModalFormConfig(params) {
    return {
      isvisible: false,
      dismissable: true,
      heading: 'Cupon',
      buttons: [{
        text: 'Save',
        type: 'success',
        action: params.onSaveClick,
      }, {
        text: 'Cancel',
        action: params.onCancelClick,
      }],
    };
  }

  getDeleteModalFormConfig(params) {
    return {
      isvisible: false,
      dismissable: true,
      heading: 'Cupon',
      buttons: [{
        text: 'Delete',
        type: 'error',
        action: params.onDeleteClick,
      }, {
        text: 'Cancel',
        action: params.onCancelClick,
      }],
    };
  }

  saveCupon(formData) {
    if (formData.validation === null) {
      const { startDate, endDate } = formData.data.period;

      store.getStore().dispatch(saveCuponAction({
        ...formData.data,
        ...{
          discountValue: formData.data.discountValue * 1,
        },
        ...{
          period: {
            startDate: startDate.format('DD-MM-YYYY'),
            endDate: endDate.format('DD-MM-YYYY'),
          },
        },
      }));
    }
  }

  getCupons() {
    return store.getStore().getState().cupon.cupons.slice();
  }

  getSortDirection(currentColumn, newColumn, currentSortDirection) {
    if (currentColumn === newColumn) {
      if (currentSortDirection === 'ASC') {
        return 'DESC';
      } else if (currentSortDirection === 'DESC') {
        return '';
      }
      return 'ASC';
    }
    return 'ASC';
  }

  isSort(sortColumnDirection, sortColumn, sortDirection) {
    const sortParts = sortColumnDirection.split('|');

    return sortParts[0] === sortColumn && sortParts[1] === sortDirection;
  }

  sortRows(rows, sortColumn, sortDirection) {
    if (sortColumn && (sortDirection !== 'undefined' && sortDirection !== '')) {
      return rows.slice().sort((left, right) => {
        if (sortColumn === 'period') {
          return sortComparePeriod(left, right, sortDirection);
        }
        return sortCompareValue(left, right, sortDirection, sortColumn);
      });
    }
    return rows;
  }

  deleteRow(uuid) {
    const cupons = this.getCupons();
    const index = cupons.findIndex(cupon => (cupon.uuid === uuid));
    cupons.splice(index, 1);
    store.getStore().dispatch(setCupons(cupons));
    return cupons;
  }

  getCupon(uuid) {
    const cupon = this.getCupons().find(iCupon => (iCupon.uuid === uuid));
    if (cupon) {
      return {
        ...cupon,
        period: {
          startDate: moment(cupon.period.startDate),
          endDate: moment(cupon.period.endDate),
        },
      };
    }
    return cupon;
  }
}

export default new CuponService();

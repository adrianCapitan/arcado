import Joi from 'joi-browser';

export default Joi.object().keys({
  uuid: Joi.string(),
  gtin: Joi.string().required(),
  name: Joi.string().min(3).max(20).required(),
  period: Joi.object().keys({
    startDate: Joi.any().required(),
    endDate: Joi.any().required(),
  }),
  discountValue: Joi.number().required(),
  status: Joi.boolean().required(),
});

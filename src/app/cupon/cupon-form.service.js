import Joi from 'joi-browser';
import moment from 'moment';
import { getFormat, validate } from 'gtin';
import joiErrorToObject from '../core/joi-error-to-object';
import cuponSchema from './cupon-schema';

class CuponFormService {
  getDefaultValue() {
    return {
      gtin: '',
      name: '',
      period: {
        startDate: moment(),
        endDate: moment().add(1, 'days'),
      },
      discountValue: 0,
      status: false,
    };
  }

  validateCupon(cupon) {
    const result = Joi.validate(cupon, cuponSchema, { abortEarly: false });
    const gtinError = this.validateGtin(cupon.gtin);

    if (result.error) {
      const errorObject = joiErrorToObject(result.error);
      errorObject.gtin = errorObject.gtin || gtinError;
      return errorObject;
    }

    if (gtinError) {
      return {
        gtin: gtinError,
      };
    }

    return result.error;
  }

  validateGtin(gtin) {
    if (!(gtin || '').startsWith('981') || !(gtin || '').startsWith('982')) {
      return { message: 'GTIN must start with 981 or 982 prefix' };
    }
    try {
      if (getFormat(gtin) !== 'GTIN-13') {
        return { message: 'Not a valid GTIN-13 format' };
      }
      if (!validate(gtin)) {
        return { message: 'Not a valid GTIN' };
      }
    } catch (e) {
      return { message: 'Not a valid GTIN-13 format' };
    }
    return null;
  }

  getStatusToggleConfig(value) {
    return {
      type: 'primary',
      checked: value,
    };
  }
}

export default new CuponFormService();

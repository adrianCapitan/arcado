import { tag } from 'riot';
import 'riotgear/dist/rg-modal/rg-modal';
import template from './cupon.tag.html';
import cuponService from './cupon.service';
import './cupon-form.tag';
import './cupon.tag.scss';

function cuponTag() {
  this.formData = {};

  this.openCuponForm = () => {
    this.modalConfig.isvisible = true;
  };

  this.onFormUpdate = (formData) => {
    this.formData = formData;
  };

  this.isSort = (sortColumnDirection) => {
    cuponService.isSort(sortColumnDirection, this.sortColumn, this.sortDirection);
  };

  this.setSort = column => () => {
    this.sortColumn = column;
    this.sortDirection = cuponService.getSortDirection(this.sortColumn, column, this.sortDirection);
    this.cupons = cuponService.sortRows(
      cuponService.getCupons(),
      this.sortColumn,
      this.sortDirection,
    );
  };

  this.deleteRow = uuid => () => {
    this.nextDelete = cuponService.getCupon(uuid);
    this.deleteModalConfig.isvisible = true;
  };

  this.editRow = uuid => () => {
    this.currentCupon = cuponService.getCupon(uuid);
    this.openCuponForm();
  };

  const getRows = () => {
    this.cupons = cuponService.sortRows(
      cuponService.getCupons(),
      this.sortColumn,
      this.sortDirection,
    );
  };

  const onSaveClick = () => {
    cuponService.saveCupon(this.formData);

    if (this.formData.validation === null) {
      this.modalConfig.isvisible = false;
      getRows();
      this.update();
    }
  };

  const onDeleteClick = () => {
    cuponService.deleteRow(this.nextDelete.uuid);
    this.deleteModalConfig.isvisible = false;
    getRows();
    this.update();
  };

  const onCancelClick = () => {
    this.modalConfig.isvisible = false;
    this.deleteModalConfig.isvisible = false;
  };

  this.modalConfig = cuponService.getModalFormConfig({
    onSaveClick,
    onCancelClick,
  });

  this.deleteModalConfig = cuponService.getDeleteModalFormConfig({
    onDeleteClick,
    onCancelClick,
  });

  getRows();
}

tag('acs-cupon', template, '', '', cuponTag);

import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import persistState from 'redux-localstorage';

/**
 * Creates a new redux store
 * @param key string value
 * @param reducer redux
 * @param initialState
 * @returns reducer redux
 */
function createStoreWithMiddleware() {
  const middlewares = applyMiddleware(thunk);

  return createStore(
    combineReducers(this.reducers),
    undefined,
    compose(middlewares, persistState()),
  );
}

class Store {
  constructor() {
    this.reducers = {};
  }

  registerReducer(key, reducer) {
    this.reducers[key] = reducer;
  }

  createStore() {
    this.store = createStoreWithMiddleware.bind(this)();
    return this.store;
  }

  getStore() {
    return this.store;
  }
}

export default new Store();

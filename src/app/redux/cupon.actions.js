import uuid from 'uuid/v1';

function setCupons(cupons) {
  return (dispatch) => {
    dispatch({
      type: 'SET_CUPONS',
      cupons,
    });
  };
}

function saveCupon(cupon) {
  return (dispatch, getState) => {
    const { cupons } = getState().cupon;
    if (cupon.uuid) {
      dispatch(setCupons(cupons.map((iCupon) => {
        if (cupon.uuid === iCupon.uuid) {
          return cupon;
        }
        return iCupon;
      })));
    } else {
      const newCupon = {
        uuid: uuid(),
        ...cupon,
      };
      dispatch(setCupons(cupons.concat([newCupon])));
    }
  };
}

export { setCupons, saveCupon };

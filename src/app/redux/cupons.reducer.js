export default (state = { cupons: [] }, action) => {
  switch (action.type) {
    case 'SET_CUPONS':
      return { cupons: action.cupons };
    default:
      return state;
  }
};

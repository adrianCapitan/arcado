import store from './store';
import cuponReducer from './cupons.reducer';
import { saveCupon, setCupons } from './cupon.actions';

export default store;
export { cuponReducer, saveCupon, setCupons };
